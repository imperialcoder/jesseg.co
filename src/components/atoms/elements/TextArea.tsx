import React from 'react';
import { animated, useSpring } from 'react-spring';
import styled from 'styled-components';

const Component = styled(animated.textarea)`
  padding: 1rem;
  background: ${(props) => props.theme.color.background[2]};
  color: ${(props) => props.theme.color.text};
  font-family: ${(props) => props.theme.font.sanSerif};
  font-size: 1.1rem;
  border: none;
  resize: none;
  border-radius: 1rem;
  outline: none;
  margin-bottom: 1rem;
`;

export type TextAreaProps = {
  placeholder?: string;
};

const TextArea = ({ placeholder }: TextAreaProps): JSX.Element => {
  const textAreaSpring = useSpring({});

  return <Component placeholder={placeholder} style={textAreaSpring} />;
};

export default TextArea;
