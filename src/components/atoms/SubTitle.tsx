import React from 'react';
import styled from 'styled-components';
import Title from 'atoms/elements/Title';

const Component = styled(Title)`
  font-size: 1.8rem;
`;

const SubTitle: typeof Title = (props) => {
  return <Component {...props} />;
};

export default SubTitle;
