import React from 'react';
import { useState } from 'react';
import styled from 'styled-components';
import { useSpring, animated } from 'react-spring';

const Component = styled(animated.a)<{ size: number }>`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  width: ${(props) => `${props.size}em`};
  height: ${(props) => `${props.size}em`};
  font-size: ${(props) => `${props.size / 3}em`};
  margin: 0.5rem;
  background: white;
  text-decoration: none;
  font-size: 1rem;
`;

const Icon = styled(animated.i)`
  font-size: 150%;
`;

export type SocialIconProps = {
  color: string;
  link: string;
  className?: string;
  size?: number;
  openNewWindow?: boolean;
  iconFile?: string;
};

const SocialIcon = ({
  color,
  link,
  className,
  size,
  openNewWindow,
  iconFile,
}: SocialIconProps): JSX.Element => {
  const [clickedState, setClickedState] = useState(false);
  const [hoverState, setHoverState] = useState(false);

  const eventHandler = (event: any) => {
    switch (event.type) {
      case 'mouseenter':
      case 'mouseleave':
        setHoverState(!hoverState);
        break;
      case 'mousedown':
        setClickedState(true);
        break;
      case 'mouseup':
        setTimeout(() => setClickedState(false), 500);
        break;
    }
  };

  const componentSpring = useSpring({
    transform: hoverState ? 'scale(1.2)' : 'scale(1)',
    boxShadow: clickedState ? '0px 0px 0px black' : '1px 0px 5px black',
    from: {
      transform: 'scale(1)',
      boxShadow: '1px 0px 5px black',
    },
  });

  const iconSpring = useSpring({
    transform: hoverState ? 'rotate(360deg)' : 'rotate(0deg)',
    from: {
      transform: 'rotate(0deg)',
    },
  });

  return (
    <Component
      style={{ ...componentSpring, color }}
      href={link}
      target={openNewWindow ? '_blank' : undefined}
      size={size || 1}
      onMouseEnter={eventHandler}
      onMouseLeave={eventHandler}
      onMouseDown={eventHandler}
      onMouseUp={eventHandler}
    >
      <Icon className={className} style={iconSpring} />
    </Component>
  );
};

export default SocialIcon;
