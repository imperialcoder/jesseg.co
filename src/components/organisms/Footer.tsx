import React from 'react';
import styled from 'styled-components';

import Title from 'atoms/elements/Title';

import SocialIcon from 'atoms/SocialIcon';

const Component = styled.footer`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1rem;
  background: ${(props) => props.theme.color.background[2]};
  box-shadow: ${(props) => props.theme.raised};
`;

const Mantra = styled(Title)`
  ${(props) => props.theme.media.desktop} {
    font-size: 2.5rem;
  }

  ${(props) => props.theme.media.phone} {
    font-size: 2.2rem;
  }
`;

const SocialContainer = styled.div`
  display: flex;
  justify-content: space-evenly;
  margin-top: 0.5rem;
`;

const SocialAccounts: {
  color: string;
  className: string;
  link: string;
  openNewWindow?: boolean;
}[] = [
  {
    color: '#55acee',
    className: 'fab fa-twitter',
    link: 'https://twitter.com/imperialcoder',
    openNewWindow: true,
  },
  {
    color: '#4875B4',
    className: 'fab fa-linkedin-in',
    link: 'https://www.linkedin.com/in/jesse-g-290675109/',
    openNewWindow: true,
  },
  {
    color: '#609926',
    className: 'fas fa-code-branch',
    link: 'https://gitlab.com/jeg6187',
    openNewWindow: true,
  },
  {
    color: '#678880',
    className: 'fas fa-envelope',
    link: 'mailto:hello@jesseg.co',
  },
  {
    color: '#ff8c1a',
    className: 'fas fa-phone-alt',
    link: 'tel:435-503-6187',
  },
];

const Footer = (): JSX.Element => {
  return (
    <Component>
      <Mantra>Let's Go Places!</Mantra>
      <SocialContainer>
        {SocialAccounts.map((account, index) => (
          <SocialIcon {...account} key={index} size={2.5} />
        ))}
      </SocialContainer>
    </Component>
  );
};

export default Footer;
