import React from 'react';
import ReactToPrint from 'react-to-print';
import styled from 'styled-components';

import Section from 'templates/Resume/GridItem';
import Button from 'atoms/elements/Button';
import Title from 'atoms/elements/Title';
import Icon from 'atoms/elements/Icon';

const Component = styled(Section)`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  grid-column: 1 / span 3;

  @media print {
    button {
      display: none;
    }
  }
`;

const ContactInfo = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;

  a {
    display: flex;
    color: ${(props) => props.theme.color.primary[2]};
    margin-bottom: 0.25rem;
    text-decoration: none;
    transition: color 250ms;

    &:hover {
      color: ${(props) => props.theme.color.primary[1]};
    }

    div {
      &:hover {
        text-decoration: underline;
      }
      @media print {
        text-decoration: underline;
        color: link;
      }
    }

    i {
      margin-left: 0.5rem;
    }
  }
`;

const ContactItems: { iconClass: string; label: string; href: string }[] = [
  {
    iconClass: 'fas fa-phone-alt',
    label: '(435) 503-6187',
    href: 'tel:435-503-6187',
  },
  {
    iconClass: 'fas fa-envelope',
    label: 'hello@jesseg.co',
    href: 'mailto:hello@jesseg.co',
  },
  {
    iconClass: 'fas fa-globe-americas',
    label: 'jesseg.co',
    href: 'https://jesseg.co/',
  },
];

export type HeaderProps = {
  printRef: React.RefObject<React.ReactInstance>;
};

const Header = (props: HeaderProps): JSX.Element => {
  return (
    <Component>
      <Title>JESSE GARCIA</Title>
      <ReactToPrint
        trigger={(): any => <Button iconClass="fas fa-print">Print</Button>}
        content={() => props.printRef.current}
      />
      <ContactInfo>
        {ContactItems.map((item, index) => (
          <a href={item.href} key={index}>
            <div>{item.label}</div>
            <Icon iconName={item.iconClass} />
          </a>
        ))}
      </ContactInfo>
    </Component>
  );
};

export default Header;
