/* eslint-disable no-undef */

// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/reference/configuration

/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  mount: {
    public: { url: '/', static: true },
    src: { url: '/dist' },
  },
  plugins: [
    '@snowpack/plugin-sass',
    '@snowpack/plugin-react-refresh',
    '@snowpack/plugin-dotenv',
    [
      '@canarise/snowpack-eslint-plugin',
      {
        globs: ['src/**/*.tsx', 'src/**/*.ts'],
      },
    ],
  ],
  alias: {
    '@': 'src',
    atoms: './src/components/atoms',
    molecules: './src/components/molecules',
    organisms: './src/components/organisms',
    templates: './src/components/templates',
    functions: './src/functions',
    pages: './src/pages',
    styles: './src/styles',
    types: './src/types',
  },
};
